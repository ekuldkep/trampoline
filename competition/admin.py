from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin

from competition.models import Attempt, AgeGroup, Participation, Competition, User, Event, AttemptEvent, PersonInRole, \
    Club, RoleInParticipation, JudgeInEvent


class EventAdmin(admin.ModelAdmin):
    list_display = ["name", "country", "location", "begin_time", "end_time", "diagonal_machine_exists"]
    list_filter = ["country", "diagonal_machine_exists"]
    search_fields = ["name", "country", "location"]


admin.site.register(Event, EventAdmin)
admin.site.register(AgeGroup)
admin.site.register(Competition)
admin.site.register(Participation)
admin.site.register(Attempt)
admin.site.register(AttemptEvent)
admin.site.register(PersonInRole)
admin.site.register(Club)
admin.site.register(RoleInParticipation)
admin.site.register(JudgeInEvent)

admin.site.register(User, UserAdmin)
