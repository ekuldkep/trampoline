# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-27 14:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('competition', '0015_auto_20170827_1342'),
    ]

    operations = [
        migrations.AddField(
            model_name='competition',
            name='judges',
            field=models.ManyToManyField(related_name='competitions', to='competition.PersonInRole'),
        ),
    ]
