"""Trampoline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from competition.views import IndexView, EventDetailView, EventCreateView, EventUpdateView, EventDeleteView, \
    CompetitionCreateView, CompetitionUpdateView, CompetitionDeleteView, EventManagementView, JudgeAddView, \
    JudgeRegisterView, CompetitionJudgeRegister, competition_participation_register_view, CompetitionDetailView, \
    attempt_event_view, ClubCreateView, ClubListView, ClubDetailView, ClubUpdateView, participation_attempt_create

urlpatterns = [
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^event/create', EventCreateView.as_view(), name="event_create"),
    url(r'^event/(?P<pk>\d+)$', EventDetailView.as_view(), name="event_detail"),
    url(r'^event/(?P<pk>\d+)/management$', EventManagementView.as_view(), name="event_management"),
    url(r'^event/(?P<pk>\d+)/update', EventUpdateView.as_view(), name="event_update"),
    url(r'^event/(?P<pk>\d+)/delete', EventDeleteView.as_view(), name="event_delete"),
    url(r'^event/(?P<event_pk>\d+)/create/competition', CompetitionCreateView.as_view(), name="competiton_create"),
    url(r'^event/(?P<event_pk>\d+)/add/judge', JudgeAddView.as_view(), name="judge_add"),
    url(r'^event/(?P<event_pk>\d+)/register/judge', JudgeRegisterView.as_view(), name="judge_register"),

    url(r'^competition/(?P<pk>\d+)/update', CompetitionUpdateView.as_view(), name="competition_update"),
    url(r'^competition/(?P<pk>\d+)/delete', CompetitionDeleteView.as_view(), name="competition_delete"),
    url(r'^competition/(?P<competition_pk>\d+)/register/judge', CompetitionJudgeRegister.as_view(), name="competition_judge_register"),
    url(r'^competition/(?P<competition_pk>\d+)/register/participation', competition_participation_register_view, name="competition_participation_register"),
    url(r'^competition/(?P<pk>\d+)$', CompetitionDetailView.as_view(), name="competition_detail"),

    url(r'^judge/(?P<pk>\d+)$', attempt_event_view, name="judge_attempt"),
    url(r'^participation/(?P<participation_pk>\d+)/create/attempt', participation_attempt_create, name="create_participation_attempt"),

    url(r'^club$', ClubListView.as_view(), name="club_list"),
    url(r'^club/create$', ClubCreateView.as_view(), name="club_create"),
    url(r'^club/(?P<pk>\d+)$', ClubDetailView.as_view(), name="club_detail"),
    url(r'^club/(?P<pk>\d+)/update$', ClubUpdateView.as_view(), name="club_update"),
]


