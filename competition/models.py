from random import randint

from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    """
    Non interactive users (registered competitors)
    are registered as ID code as their username
    """

    id_code = models.CharField(max_length=30, blank=True)
    country = models.CharField(max_length=128, blank=True)

    date_of_birth = models.DateField(blank=True, null=True)

    REQUIRED_FIELDS = AbstractUser.REQUIRED_FIELDS

    def __str__(self):
        return "{self.first_name} {self.last_name} {self.id_code}".format(self=self)


class PersonInRole(models.Model):
    NoType = 0
    Competitor = 1
    Coach = 2
    DifficultyJudge = 3  # is very difficult to deal with
    DiagonalJudge = 4  # lays on on the floor diagonally like a cat
    TimeJudge = 5  # doctor who
    SyncroJudge = 6  # two asians pretending to be a single judge
    HeadJudge = 7  # judges head shape
    ExecutionJudge = 8
    Reduce = 9
    Admin = 10

    ROLE_CHOICES = [
        (NoType, "NoType"),
        (Competitor, "Competitor"),
        (Coach, "Coach"),
        (DifficultyJudge, "DifficultyJudge"),
        (DiagonalJudge, "DiagonalJudge"),
        (TimeJudge, "TimeJudge"),
        (SyncroJudge, "SyncroJudge"),
        (HeadJudge, "HeadJudge"),
        (ExecutionJudge, "ExecutionJudge"),
        (Reduce, "Reduce"),
        (Admin, "Admin"),
    ]

    user = models.ForeignKey(User, related_name="roles")
    role = models.SmallIntegerField(choices=ROLE_CHOICES)

    def __str__(self):
        return self.user.get_full_name() + " " + self.get_role_display()


class Club(models.Model):
    name = models.CharField(max_length=150)
    location = models.CharField(max_length=200)
    country = models.CharField(max_length=128)
    staff = models.ManyToManyField(User, related_name="staffed_clubs")

    description = models.TextField(blank=True)

    email = models.EmailField()

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=128)
    country = models.CharField(max_length=128)
    location = models.CharField(max_length=128)
    description = models.TextField()
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    diagonal_machine_exists = models.BooleanField()

    leaflet = models.FileField(blank=True, null=True)
    image = models.ImageField(blank=True, null=True)

    # by default this will be filled with the hosting clubs staff
    staff = models.ManyToManyField(User, blank=True, related_name="staffed_events")
    club = models.ForeignKey(Club, blank=True, null=True)

    def is_staff(self, user):
        return self.staff.filter(id=user.id).exists()

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # default the staff to club if it exists
        if not self.pk and self.club:
            self.staff = list(self.club.staff) + list(self.staff)
        super().save(force_insert, force_update, using, update_fields)


class JudgeInEvent(models.Model):
    person = models.ForeignKey(User, related_name="judged_events")
    event = models.ForeignKey(Event, related_name="available_judges")
    participation_comment = models.CharField(max_length=128, blank=True)
    availability_comment = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return str(self.person)


class AgeGroup(models.Model):
    age_group = models.CharField(max_length=50)

    def __str__(self):
        return self.age_group


class Competition(models.Model):
    NoType = 0
    Synchronized = 1
    Individual = 2

    TYPE_CHOICES = [
        (NoType, "No Type"),
        (Synchronized, "Synchronized"),
        (Individual, "Individual"),
    ]

    type = models.IntegerField(choices=TYPE_CHOICES)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="competitions")
    age_group = models.ForeignKey(AgeGroup, on_delete=models.CASCADE, related_name="competitions")
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    gender = models.CharField(max_length=128)  # fred thought this

    judges = models.ManyToManyField(PersonInRole, related_name="competitions", blank=True)

    def __str__(self):
        return "{} {} {}".format(self.age_group, self.get_type_display(), self.gender)

    def available_roles(self):
        desired_roles = {
            PersonInRole.DifficultyJudge: 2,
            PersonInRole.DiagonalJudge: 2,
            PersonInRole.HeadJudge: 1,
            PersonInRole.ExecutionJudge: 4,
        }

        if self.event.diagonal_machine_exists:
            del desired_roles[PersonInRole.DiagonalJudge]
            desired_roles[PersonInRole.ExecutionJudge] = 6

        for role, count in list(desired_roles.items()):
            if self.judges.filter(role=role).count() >= count:
                del desired_roles[role]
            else:
                desired_roles[role] -= self.judges.filter(role=role).count()

        return desired_roles

    def available_role_choices(self):
        desirable_roles = self.available_roles()
        return [choice for choice in PersonInRole.ROLE_CHOICES if choice[0] in desirable_roles]


def magic_generator():
    return randint(0, 2**30)


class Participation(models.Model):
    place = models.IntegerField(blank=True, null=True)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE, related_name="participations")
    magic_number = models.IntegerField(default=magic_generator)

    class Meta:
        ordering = ("magic_number", "id")

    def in_progress_attempt(self):
        return self.attempts.filter(closed=False).first()

    def coaches(self):
        return self.role_in_participations.filter(person_in_role__role=PersonInRole.Coach)

    def participants(self):
        return self.role_in_participations.filter(person_in_role__role=PersonInRole.Competitor)

    def participants_name(self):
        return ", ".join(
            "{} {}".format(participant.person_in_role.user.get_full_name(), participant.club)
            for participant in self.participants()
        )

    def __str__(self):
        return self.participants_name()


class Attempt(models.Model):
    order = models.IntegerField(null=True, blank=True)
    final = models.BooleanField(default=False)
    closed = models.BooleanField(default=False)
    points_total = models.FloatField(default=0, blank=True)
    participation = models.ForeignKey(Participation, on_delete=models.CASCADE, related_name="attempts")

    def __str__(self):
        return "{} [{}]".format(self.participation, self.order)

    def judging_configuration(self, user):
        competition = self.participation.competition
        judge = competition.judges.filter(user=user).first()
        judge_type = judge.role

        individual = competition.type == Competition.Individual
        synchro = competition.type == Competition.Synchronized
        diagonal = competition.event.diagonal_machine_exists  # move to competition

        conf = {
            PersonInRole.HeadJudge:
                [AttemptEvent.JudgePenalty]
                + [AttemptEvent.Diagonal] * diagonal
                + [AttemptEvent.Synchronized] * synchro
                + [AttemptEvent.Time] * individual,
            PersonInRole.DifficultyJudge:
                [AttemptEvent.Difficulty],
            PersonInRole.DiagonalJudge:
                [AttemptEvent.Diagonal],
            PersonInRole.ExecutionJudge:
                [AttemptEvent.Execution],
        }

        testing_fallback = [AttemptEvent.JudgePenalty, AttemptEvent.Synchronized]
        return conf.get(judge_type) or testing_fallback


class AttemptEvent(models.Model):
    NoType = 1
    Difficulty = 2
    Diagonal = 3
    Time = 4
    Synchronized = 5
    Execution = 6
    JudgePenalty = 7
    Reduce = 8

    POINTS_TYPE_CHOICES = [
        (NoType, "NoType"),
        (Difficulty, "Difficulty"),
        (Diagonal, "Diagonal"),
        (Time, "Time"),
        (Synchronized, "Synchronized"),
        (Execution, "Execution"),
        (JudgePenalty, "JudgePenalty"),
        (Reduce, "Reduce"),
    ]

    creator = models.ForeignKey(PersonInRole)
    attempt = models.ForeignKey(Attempt, related_name="attempevent")
    points = models.FloatField()
    points_type = models.SmallIntegerField(choices=POINTS_TYPE_CHOICES)

    def __str__(self):
        return "{} {}".format(self.creator, self.get_points_type_display())


class RoleInParticipation(models.Model):
    person_in_role = models.ForeignKey(PersonInRole, related_name="role_in_participations")
    participation = models.ForeignKey(Participation, related_name="role_in_participations")
    club = models.ForeignKey(Club, related_name="role_in_participations")

    def __str__(self):
        return "{} - {}".format(self.person_in_role, self.club)
