from datetime import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.contrib.auth.mixins import LoginRequiredMixin
from django import forms
from django.db.models.aggregates import Count
from django.forms.formsets import formset_factory
from django.forms.models import ModelForm
from django.forms.widgets import HiddenInput
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls.base import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic.base import TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, ModelFormMixin
from django.views.generic.list import ListView

from competition.models import Event, Competition, JudgeInEvent, User, PersonInRole, Club, Participation, \
    RoleInParticipation, AttemptEvent, Attempt


class UserModelRelationMixin:
    user_related_by = "staff"

    def get_queryset(self):
        user = self.request.user
        # basically transforms to .filter(staff=user) when user_related_by = "staff"
        return super().get_queryset().filter(**{self.user_related_by: user})


class EventStaffMixin(UserModelRelationMixin):
    user_related_by = "staff"


class CompetitionStaffMixin(UserModelRelationMixin):
    user_related_by = "event__staff"


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        events_grouped = []
        events = Event.objects.all()  # filter(begin_time__gte=datetime.now())

        while events:
            triplet = events[:3]
            events_grouped.append(triplet)

            events = events[3:]

        context["events_grouped"] = events_grouped
        return context


class EventDetailView(LoginRequiredMixin, DetailView):
    model = Event
    template_name = "event_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_staff"] = self.object.is_staff(self.request.user)
        return context


class EventManagementView(LoginRequiredMixin, EventStaffMixin, DetailView):
    model = Event
    template_name = "event_management.html"


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = "__all__"

    staff = forms.ModelMultipleChoiceField(
        required=True,
        queryset=User.objects.all(),
    )

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    template_name = "base_create.html"
    form_class = EventForm

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.pk})


class EventUpdateView(LoginRequiredMixin, EventStaffMixin, UpdateView):
    model = Event
    title = "Update event"
    template_name = "base_create.html"
    form_class = EventForm

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.pk})


class EventDeleteView(LoginRequiredMixin, EventStaffMixin, DeleteView):
    model = Event
    title = "Delete event"
    template_name = "base_delete.html"
    success_url = reverse_lazy('index')

    # TODO: when should we be able to deletus Event, when not running perhaps?


class CompetitionForm(ModelForm):
    class Meta:
        model = Competition
        exclude = ['judges']
        widgets = {'event': HiddenInput()}

    def clean_begin_time(self):
        begin_time = self.cleaned_data['begin_time']
        if begin_time < timezone.now():
            raise forms.ValidationError("Event can't happen in the past")
        return begin_time

    def clean_end_time(self):
        end_time = self.cleaned_data['end_time']
        if end_time < timezone.now():
            raise forms.ValidationError("Event can't happen in the past")

        if end_time <= self.cleaned_data['begin_time']:
            raise forms.ValidationError("Event can't end before beginning")

        return end_time

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))


class CompetitionCreateView(LoginRequiredMixin, CompetitionStaffMixin, CreateView):
    model = Competition
    form_class = CompetitionForm
    title = "Create competition"
    template_name = "base_create.html"

    def get_form(self):
        form = super().get_form(self.form_class)
        form.fields["event"].initial = self.kwargs["event_pk"]
        return form

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.event.pk})


class CompetitionUpdateView(LoginRequiredMixin, CompetitionStaffMixin, UpdateView):
    model = Competition
    form_class = CompetitionForm
    title = "Update competition"
    template_name = "base_create.html"

    def get_form(self, **kwargs):
        form = super().get_form(self.form_class)
        del form.fields["event"]
        return form

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.event.pk})


class CompetitionDeleteView(LoginRequiredMixin, CompetitionStaffMixin, DeleteView):
    model = Competition
    title = "Delete competition"
    template_name = "base_delete.html"

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.event.pk})


class JudgeInEventForm(ModelForm):
    class Meta:
        model = JudgeInEvent
        fields = "__all__"
        widgets = {'event': HiddenInput()}

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))


class JudgeAddView(LoginRequiredMixin, CreateView):
    model = JudgeInEvent
    title = "Add judge to Event"
    template_name = "base_create.html"
    form_class = JudgeInEventForm

    def get_form(self, **kwargs):
        form = super().get_form(self.form_class)
        form.fields["event"].initial = self.kwargs["event_pk"]
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sub_link"] = reverse("judge_register", kwargs={'event_pk': self.kwargs["event_pk"]})
        context["sub_link_text"] = "Maybe create judge?"
        return context

    def get_success_url(self):
        return reverse('event_management', kwargs={'pk': self.object.event.pk})


class JudgeInEventCreateForm(ModelForm):
    class Meta:
        model = JudgeInEvent
        fields = "__all__"
        exclude = ["person"]
        widgets = {'event': HiddenInput()}

    id_code = forms.CharField(max_length=128)
    country = forms.CharField()

    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))

    def clean_id_code(self):
        id_code = self.cleaned_data['id_code']

        if User.objects.filter(id_code=id_code).exists():
            raise forms.ValidationError("You have forgotten about Fred!")
        return id_code

    def save(self, commit=True):
        date = None
        try:
            date = datetime.strptime(self.cleaned_data["id_code"][1:7], "%y%m%d")
        except:
            pass

        # TODO maybe should use get or create, you dumbfucks
        user = User(
            id_code=self.cleaned_data["id_code"],
            username=self.cleaned_data["id_code"],
            country=self.cleaned_data["country"],
            first_name=self.cleaned_data["first_name"],
            last_name=self.cleaned_data["last_name"],
            date_of_birth=date,
        )

        if commit:
            user.save()
        self.instance.person = user
        return super().save(commit)


class JudgeRegisterView(JudgeAddView):
    form_class = JudgeInEventCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sub_link"] = reverse("judge_add", kwargs={'event_pk': self.kwargs["event_pk"]})
        context["sub_link_text"] = "Maybe add an existing judge?"
        return context


class CompetitionJudgeForm(ModelForm):
    class Meta:
        model = PersonInRole
        fields = "__all__"

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))

    def __init__(self, *args, **kwargs):
        self.competition = kwargs.pop("competition")
        super().__init__(*args, **kwargs)

        self.fields["role"].choices = self.competition.available_role_choices()

        # judges registered to event
        available_judges = self.competition.event.available_judges.all()  # A = {1, 2, 3, 4, 5}

        # fetch the current competition up to date judges
        current_judges_for_comp = self.competition.judges.values_list("user_id", flat=True)  # B = {1, 2, 5}

        # all judges that do not work in this competition
        available_judges = available_judges.exclude(person_id__in=current_judges_for_comp)  # A - B = {

        # all users who should be selectable
        self.fields["user"].queryset = User.objects.filter(id__in=available_judges.values_list("person_id", flat=True))

    def save(self, commit=True):
        if commit:
            self.instance, created = PersonInRole.objects.get_or_create(user=self.instance.user,
                                                                        role=self.instance.role)
            self.competition.judges.add(self.instance)
            self.competition.save()
        return self.instance


class CompetitionJudgeRegister(LoginRequiredMixin, CreateView):
    model = PersonInRole
    title = "Add judge to Competition"
    template_name = "judge_register.html"
    form_class = CompetitionJudgeForm

    def get_competition(self):
        return get_object_or_404(Competition, pk=self.kwargs["competition_pk"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["back_link"] = reverse("event_management", kwargs={'pk': self.get_competition().event_id})
        context["registered_judges"] = self.get_competition().judges.all()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["competition"] = self.get_competition()
        return kwargs

    def get_success_url(self):
        competition = self.get_competition()
        return reverse('event_management', kwargs={'pk': competition.event.pk})


class ParticipationForm(ModelForm):
    class Meta:
        model = User
        fields = ("id_code", "country", "first_name", "last_name")

    club = forms.ModelChoiceField(queryset=Club.objects.all())

    def save(self, commit=True, participation=None):
        user_data = self.cleaned_data.copy()
        club = user_data.pop("club")
        user_data["username"] = self.cleaned_data["id_code"]

        user, created = User.objects.get_or_create(
            id_code=user_data["id_code"],
            defaults=user_data,
        )

        person_in_role, created = PersonInRole.objects.get_or_create(user=user, role=PersonInRole.Competitor)

        role_in_participation = RoleInParticipation.objects.create(
            person_in_role=person_in_role,
            participation=participation,
            club=club,
        )


class ExampleFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ExampleFormSetHelper, self).__init__(*args, **kwargs)
        self.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))
        self.template = 'bootstrap/table_inline_formset.html'


def competition_participation_register_view(request, competition_pk):
    title = "Add participants to Competition"
    competition = get_object_or_404(Competition, pk=competition_pk)

    initial_club = request.COOKIES.get("club", None)

    count = 2
    formset = formset_factory(ParticipationForm, max_num=count, min_num=count)
    form = formset(data=request.POST or None, initial=[{"club": initial_club}] * count)
    form.helper = ExampleFormSetHelper()

    if request.method == 'POST' and form.is_valid():
        participation = Participation.objects.create(competition=competition)

        for sub_form in form:
            sub_form.save(participation=participation)

        success_url = reverse('competition_participation_register', kwargs={'competition_pk': competition_pk})
        response = HttpResponseRedirect(success_url)
        response.set_cookie("club", form[-1].cleaned_data["club"])
        return response

    return render(request, "base_create.html",
                  {'form': form, 'title': title, 'request': request, 'object_list': Participation.objects.all(),
                   'heading': 'Participations'})


class CompetitionDetailView(LoginRequiredMixin, DetailView):
    model = Competition
    template_name = "competition_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["participations"] = self.object.participations.annotate(attempt_count=Count("attempts")).order_by(
            "attempt_count")

        print(self.object.participations.filter(attempts__closed=False).count())
        context["current_participation"] = self.object.participations.filter(attempts__closed=False).first()
        return context


class AttemptEventForm(ModelForm):
    class Meta:
        model = AttemptEvent
        fields = ["points", "points_type"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["points_type"].disabled = True


def attempt_event_view(request, pk):
    title = "Judge"
    attempt = get_object_or_404(Attempt, pk=pk)
    competition = attempt.participation.competition
    user = request.user
    person_in_role = competition.judges.filter(user=user).first()

    configuration = attempt.judging_configuration(user)

    count = len(configuration)

    formset = formset_factory(AttemptEventForm, max_num=count, min_num=count)
    form = formset(data=request.POST or None, initial=[{"points_type": pt} for pt in configuration])
    form.helper = ExampleFormSetHelper()

    if request.method == 'POST' and form.is_valid():
        for sub_form in form:
            attempt_event = sub_form.instance
            attempt_event.attempt = attempt
            attempt_event.creator = person_in_role
            attempt_event.save()

        success_url = reverse('competition_detail', kwargs={'pk': competition.pk})
        response = HttpResponseRedirect(success_url)
        return response

    return render(request, "base_create.html", {'form': form, 'title': title, 'request': request})


def participation_attempt_create(request, participation_pk):
    participation = Participation.objects.filter(pk=participation_pk).first()
    competition = participation.competition
    open_attempt_exists = competition.participations.filter(attempts__closed=False).exists()

    if request.method == 'POST' and not open_attempt_exists:
        attempt = Attempt.objects.create(
            participation=participation,
            order=participation.attempts.count() + 1,
        )

        redirect = reverse('judge_attempt', kwargs={'pk': attempt.pk})
        return HttpResponseRedirect(redirect)

    redirect = reverse('competition_detail', kwargs={'pk': participation_pk})
    return HttpResponseRedirect(redirect)


class ClubForm(ModelForm):
    class Meta:
        model = Club
        fields = "__all__"

    staff = forms.ModelMultipleChoiceField(
        required=True,
        queryset=User.objects.all(),
    )

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Confirm', css_class='btn-lg btn-success'))


class ReadOnlyModelFormMixin(ModelFormMixin):
    """
    Renders all model fields as un-editable form, lol :D
    """

    def get_form(self, form_class=None):
        form = super(ReadOnlyModelFormMixin, self).get_form()

        for field in form.fields:
            # Set html attributes as needed for all fields
            form.fields[field].widget.attrs['readonly'] = 'readonly'
            form.fields[field].widget.attrs['disabled'] = 'disabled'

        return form

    def form_valid(self, form):
        return self.form_invalid(form)


class ClubCreateView(LoginRequiredMixin, CreateView):
    model = Club
    template_name = "base_create.html"
    form_class = ClubForm

    def get_success_url(self):
        return reverse('club_list')


class ClubListView(LoginRequiredMixin, ListView):
    model = Club
    template_name = 'generic_list_view.html'

    def get_context_data(self, **kwargs):
        context = super(ClubListView, self).get_context_data(**kwargs)
        context['title'] = 'Clubs'
        context['heading'] = 'Clubs'
        context['update_url_name'] = 'club_update'
        context['detail_url_name'] = 'club_detail'
        return context


class ClubUpdateView(LoginRequiredMixin, UpdateView):
    model = Club
    title = "Update club"
    template_name = "base_create.html"
    form_class = ClubForm

    def get_success_url(self):
        return reverse('club_list')


class ClubDetailView(LoginRequiredMixin, ReadOnlyModelFormMixin, UpdateView):
    model = Club
    title = "Detail club"
    template_name = "base_create.html"
    fields = '__all__'
